package main

import "fmt"

type Course struct {
	Name  string
	Price int
	url   string
}

func (a *Course) set_Price() {
	a.Price = 100
}

func main() {
	s := [5]int{1, 3, 2, 5, 3}
	slice1 := s[1:3]
	slice2 := slice1[:]
	fmt.Println(slice2)
	slice2 = append(slice2, 9)
	fmt.Println(cap(slice2))
	fmt.Println(slice2)
	fmt.Println(slice1)
	//s, _ := strconv.ParseFloat("3.1415", 64)
	//fmt.Printf("%T", s)

	//s := strconv.FormatBool(true)
	//fmt.Println(s)
	//s := strconv.FormatFloat(3.1415, 'E', -1, 64)
	//fmt.Println(s)
	//s := strconv.FormatInt(-42, 16) //表示将-42转换为16进制数，转换的结果为-2a。
	//s := strconv.FormatUint(42, 10)
	//fmt.Println(s)
	var c Course = Course{
		Name:  "flask",
		Price: 17,
		url:   "www.baidu.com",
	}
	c.set_Price()
	fmt.Println(c.Price)

}
