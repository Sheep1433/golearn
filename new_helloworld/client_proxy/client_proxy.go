package client_proxy

import (
	"learn/new_helloworld/handler"
	"net/rpc"
)

type HelloServiceStub struct {
	*rpc.Client
}

func NewHelloServiceClient(protol, address string) HelloServiceStub {
	client, err := rpc.Dial(protol, address)
	if err != nil {
		panic("connect error!")
	}
	return HelloServiceStub{client}
}
func (c *HelloServiceStub) Hello(request string, reply *string) error {
	err := c.Call(handler.HelloServiceName+".hello", request, reply)
	if err != nil {
		return err
	}
	return nil

}
