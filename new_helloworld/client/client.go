package main

import (
	"fmt"

	"learn/new_helloworld/client_proxy"
)

func main() {
	//1.建立连接
	client := client_proxy.NewHelloServiceClient("tcp", "localhost:1234")
	var reply string
	err := client.Hello("bobby", &reply)
	//if err != nil {
	//	panic("调用失败")
	//}
	fmt.Println(err)
	fmt.Println(reply)
}
