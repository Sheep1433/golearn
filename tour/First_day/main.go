package main

import (
	"fmt"
	"strings"
)

func main() {
	s := "this is a string a is"
	res := strings.Fields(s)
	cout := make(map[string]int, 10)
	ok := false
	for _, str := range res {
		_, ok = cout[str]
		if ok {
			cout[str]++
		} else {
			cout[str] = 1
		}
	}

	fmt.Println(cout)
}
