package main

import "fmt"

func main() {
	maplist := make(map[string]int, 8)
	maplist["章正强"] = 100
	maplist["胡瑶瑛"] = 200
	v, ok := maplist["章正强"]
	if ok {
		fmt.Printf("章正强: %d\n", v)
	}
	k, ok := maplist["胡瑶瑛"]
	if ok {
		fmt.Printf("胡瑶瑛: %d\n", k)
	}
	fmt.Println(maplist)
}
