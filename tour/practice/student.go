package main

import "fmt"

type student struct {
	id    int // 学号是唯一的
	name  string
	class string
}

// newStudentMgr是studentMgr的构造函数
func newStudentMgr() *studentMgr {
	return &studentMgr{
		allStudents: make([]*student, 0, 20),
	}
}

// 学员管理的类型
type studentMgr struct {
	allStudents []*student
}

// student类型的构造函数
func newStudent(id int, name, class string) *student {
	return &student{
		id,
		name,
		class,
	}
}

// 1.添加学生
func (s *studentMgr) addStudent(newStu *student) {
	s.allStudents = append(s.allStudents, newStu)
}

// 2.编辑学生
func (s *studentMgr) modifyStudent(newStu *student) {
	for i, v := range s.allStudents {
		if newStu.id == v.id {
			s.allStudents[i] = newStu
			return
		}

	}
	// 如果走到这里说明输入的学生没有找到
	fmt.Println("输入的学生信息有误")
}

// 3.展示学生
func (s *studentMgr) showStudent() {
	for _, v := range s.allStudents {
		fmt.Printf("学号：%d 姓名：%s 班级：%s\n", v.id, v.name, v.class)
	}
}
