package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	var name string
	flag.StringVar(&name, "name", "Go 语言编程之旅", "帮助信息")
	flag.StringVar(&name, "n", "Go 语言编程之旅", "帮助信息")
	fmt.Println(os.Args[1:])
	//fmt.Printf("%T\n", os.Args[1:])
	flag.Parse()
	log.Printf("name: %s", name)
}
