package main

import (
	"encoding/json"
	"fmt"
)

// json序列化和反序列化

type Student struct {
	ID     int `json:"id"`
	Name   string
	Gender string
}

type Class struct {
	Title    string
	Students []Student
}

func NewStudent(ID int, Name, Gender string) Student {
	return Student{
		ID:     ID,
		Name:   Name,
		Gender: Gender,
	}
}

func main() {
	c := &Class{
		Title:    "101",
		Students: make([]Student, 0, 20),
	}
	for i := 0; i <= 50; i++ {
		stu := Student{
			ID:     i,
			Name:   fmt.Sprintf("stu%02d", i),
			Gender: "男",
		}
		c.Students = append(c.Students, stu)
	}

	fmt.Printf("%#v\n", c)
	fmt.Printf("%#v\n", c.Students)
	data, err := json.Marshal(c)
	if err != nil {
		fmt.Println("json marshal failed")
		return
	}
	fmt.Printf("json:%s\n", data)

	jsonstr := `{"Title":"101","Students":[{"ID":0,"Name":"stu00","Gender":"男"},{"ID":1,"Name":"stu01","Gender":"男"},{"ID":2,"Name":"stu02","Gender":"男"},{"ID":3,"Name":"stu03","Gender":"男"},{"ID":4,"Name":"stu04","Gender":"男"},{"ID":5,"Name":"stu05","Gender":"男"},{"ID":6,"Name":"stu06","Gender":"男"},{"ID":7,"Name":"stu07","Gender":"男"},{"ID":8,"Name":"stu08","Gender":"男"},{"ID":9,"Name":"stu09","Gender":"男"},{"ID":10,"Name":"stu10","Gender":"男"},{"ID":11,"Name":"stu11","Gender":"男"},{"ID":12,"Name":"stu12","Gender":"男"},{"ID":13,"Name":"stu13","Gender":"男"},{"ID":14,"Name":"stu14","Gender":"男"},{"ID":15,"Name":"stu15","Gender":"男"},{"ID":16,"Name":"stu16","Gender":"男"},{"ID":17,"Name":"stu17","Gender":"男"},{"ID":18,"Name":"stu18","Gender":"男"},{"ID":19,"Name":"stu19","Gender":"男"},{"ID":20,"Name":"stu20","Gender":"男"},{"ID":21,"Name":"stu21","Gender":"男"},{"ID":22,"Name":"stu22","Gender":"男"},{"ID":23,"Name":"stu23","Gender":"男"},{"ID":24,"Name":"stu24","Gender":"男"},{"ID":25,"Name":"stu25","Gender":"男"},{"ID":26,"Name":"stu26","Gender":"男"},{"ID":27,"Name":"stu27","Gender":"男"},{"ID":28,"Name":"stu28","Gender":"男"},{"ID":29,"Name":"stu29","Gender":"男"},{"ID":30,"Name":"stu30","Gender":"男"},{"ID":31,"Name":"stu31","Gender":"男"},{"ID":32,"Name":"stu32","Gender":"男"},{"ID":33,"Name":"stu33","Gender":"男"},{"ID":34,"Name":"stu34","Gender":"男"},{"ID":35,"Name":"stu35","Gender":"男"},{"ID":36,"Name":"stu36","Gender":"男"},{"ID":37,"Name":"stu37","Gender":"男"},{"ID":38,"Name":"stu38","Gender":"男"},{"ID":39,"Name":"stu39","Gender":"男"},{"ID":40,"Name":"stu40","Gender":"男"},{"ID":41,"Name":"stu41","Gender":"男"},{"ID":42,"Name":"stu42","Gender":"男"},{"ID":43,"Name":"stu43","Gender":"男"},{"ID":44,"Name":"stu44","Gender":"男"},{"ID":45,"Name":"stu45","Gender":"男"},{"ID":46,"Name":"stu46","Gender":"男"},{"ID":47,"Name":"stu47","Gender":"男"},{"ID":48,"Name":"stu48","Gender":"男"},{"ID":49,"Name":"stu49","Gender":"男"},{"ID":50,"Name":"stu50","Gender":"男"}]}
`
	c1 := &Class{}
	err = json.Unmarshal([]byte(jsonstr), c1)
	if err != nil {
		fmt.Println("json unmarshal failed!")
		return
	}
	fmt.Printf("%#v\n", c1)

}
