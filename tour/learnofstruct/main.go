package main

import "fmt"

// 结构体的继承

type Animal struct {
	feet int8
	age  int8
	name string
}

type Dog struct {
	tail bool
	*Animal
}

func (d *Dog) wang() {

	fmt.Printf("%s can wangwangwang\n", d.name)
}

func (a *Animal) run() {
	fmt.Printf("%s can run\n", a.name)
}

func main() {
	dog := &Dog{
		tail: true,
		Animal: &Animal{
			feet: 4,
			age:  8,
			name: "dog",
		},
	}
	dog.run()
	dog.wang()

}
