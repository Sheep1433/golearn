package main

import (
	"fmt"
	"time"
)

func main() {
	for i := 0; i < 1000000; i++ {
		go func(n int) {
			fmt.Println(n)
			time.Sleep(time.Second)
		}(i)
	}

	time.Sleep(time.Second * 30)
}
